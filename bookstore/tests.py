from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from .views import searchBook
from selenium import webdriver
import time

# Create your tests here.
class Testing(TestCase):

	def test_url(self):
		response = self.client.get("/bookstore/")
		self.assertEqual(response.status_code, 200)

	def test_method_exp(self):
		response = resolve("/bookstore/")
		self.assertEqual(response.func, searchBook)

	def test_templates(self):
		response = self.client.get("/bookstore/")
		self.assertTemplateUsed(response, "index3.html")

	def test_json_data_availability(self):
		response = self.client.get("/bookstore/data/")
		self.assertEqual(response.status_code, 200)

class Selenium(LiveServerTestCase):

	def setUp(self):
		super().setUp()
		chrome_options = webdriver.ChromeOptions()
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		chrome_options.add_argument('--disable-dev-shm-usage')
		self.sys = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')


	def tearDown(self):
		self.sys.quit()
		super().tearDown()

	def test_functional(self):
		self.sys.get('http://localhost:8000/bookstore')
		self.assertIn('Books List', self.sys.page_source)
		self.sys.find_element_by_id('input').send_keys('What')
		self.sys.find_element_by_id('tombol').click()
		time.sleep(8)
		self.assertIn('What', self.sys.page_source)