from django.urls import path
from .views import searchBook, showData

urlpatterns = [
   path('', searchBook, name = 'bookList'),
   path('data/', showData, name='books_data'),
]
