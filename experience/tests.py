from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from .views import exp
from selenium import webdriver

class Testing(TestCase):

	def test_url(self):
		response = self.client.get("/experience/")
		self.assertEqual(response.status_code,200)

	def test_method_exp(self):
		response = resolve("/experience/")
		self.assertEqual(response.func, exp)

	def test_templates(self):
		response = self.client.get("/experience/")
		self.assertTemplateUsed(response, "index2.html")

class TestSelenium(LiveServerTestCase):
	
	def setUp(self):
		super().setUp()
		chrome_options = webdriver.ChromeOptions()
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		chrome_options.add_argument('--disable-dev-shm-usage')
		self.sys = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')
		self.sys.get('http://localhost:8000/experience')

	def tearDown(self):
		self.sys.quit()
		super().tearDown()

	def test_theme(self):
	    browser = self.sys
	    body_background = browser.find_element_by_name('body-1').value_of_css_property('background-color')
	    self.assertIn("rgba(41, 52, 98, 1)", body_background)
	    switch_button = browser.find_element_by_class_name("switch")
	    switch_button.click()
	    body_background = browser.find_element_by_name('body-1').value_of_css_property('background-color')
	    self.assertIn("rgba(210, 250, 251, 1)", body_background)

	def test_activities(self):
	    browser = self.sys
	    accordion = browser.find_element_by_name('acc-1')
	    accordion.click()
	    self.assertIn("Teaching Informatics Olympiad",browser.page_source)
	    self.assertIn("Studying",browser.page_source)
	    self.assertIn("Travelling",browser.page_source)

	def test_organization(self):
	    browser = self.sys
	    accordion = browser.find_element_by_name('acc-2')
	    self.assertIn("Saimala UI",browser.page_source)

	def test_awards(self):
	    browser = self.sys
	    accordion = browser.find_element_by_name('acc-3')
	    accordion.click()
	    self.assertIn("Bronze Medalist Informatics National Science Olympiad 2017",browser.page_source)
	    self.assertIn("Top 16 Pelatnas 1 TOKI 2018",browser.page_source)
	    self.assertIn("National Bronze Medalist ACM-ICPC Regional Jakarta 2019",browser.page_source)
