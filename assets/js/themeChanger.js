$(document).ready(function() {

    $(function() {
        $("#accordion").accordion({
            collapsible: true,
            active: false,
        });
    });

    $("input").change(function() {
        if (this.checked) {
            $('link[href="/static/css/dark.css"]').attr('href', '/static/css/light.css');
            document.getElementById('light').innerHTML = '<strong>Light Mode</strong>';
        } else {
            $('link[href="/static/css/light.css"]').attr('href', '/static/css/dark.css');
            document.getElementById('light').innerHTML = '<strong>Dark Mode</strong>';
        }
    });


});
