from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from .views import stat
from .models import dataStatus
from selenium import webdriver

class Testing(TestCase):

	def test_url(self):
		response = self.client.get("/")
		self.assertEqual(response.status_code,200)

	def test_method_stat(self):
		response = resolve("/")
		self.assertEqual(response.func, stat)

	def test_templates(self):
		response = self.client.get("/")
		self.assertTemplateUsed(response, "index.html")

	def test_create_object(self):
		response = self.client.post("/", data={"isi_Status" : "coba"})
		cnt = dataStatus.objects.count()
		self.assertEqual(cnt,1)

	def test_create_empty_object(self):
		response = self.client.post("/", data={"isi_Status" : ""})
		cnt = dataStatus.objects.count()
		self.assertEqual(cnt,0)

	def test_out_object(self):
		response = self.client.post("/", data={"isi_Status" : "coba"})
		html_response = response.content.decode()
		self.assertIn(html_response, 'coba')

class TestSelenium(LiveServerTestCase):
	
	def setUp(self):
		super().setUp()
		chrome_options = webdriver.ChromeOptions()
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		chrome_options.add_argument('--disable-dev-shm-usage')
		self.sys = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')


	def tearDown(self):
		self.sys.quit()
		super().tearDown()

	def test_functional(self):
		self.sys.get('http://localhost:8000/')
		self.sys.find_element_by_name('isi_Status').send_keys('test')
		self.sys.find_element_by_name('tombol').click()
		html_response = self.sys.page_source
		self.assertIn("test", html_response)