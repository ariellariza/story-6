from django.shortcuts import render, redirect
from .forms import statForms
from .models import dataStatus

# Create your views here.
def stat(request):
	if(request.method=="POST"):
		tmp_form = statForms(request.POST)
		if(tmp_form.is_valid()):
			tmp_models = dataStatus()
			tmp_models.tulisanStatus = tmp_form.cleaned_data["isi_Status"]
			tmp_models.save()
		return redirect("/")
	else:
		tmp_form = statForms()
		tmp_models = dataStatus.objects.all()
		tmp_dic = {
			"forms" : tmp_form,
			"models" : tmp_models,
		}
		return render(request, "index.html", tmp_dic)