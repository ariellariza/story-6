from django import forms

class statForms(forms.Form):
    isi_Status = forms.CharField(widget= forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Masukkan Status',
        'type' : 'text',
        'required' : True
    }))